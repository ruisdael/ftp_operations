# Ruisdael FTP Operations Python Module

**Transfers files from source directory (from machine where script is running) to destination FTP & produces md5 hashes**

**Requirements:**  install requirements with `pip install -r requirements.txt`


**YAML configuration files files**

* (Required) [datasrcs.yml](./datasrcs.yml) contains essential information about each data source
* (Required) `config.yml` contains the FTP credentials, based on [config.yml.template](config.yml.template) ([disdrodl-playbook](https://gitlab.tudelft.nl/ruisdael/ansible-playbooks/disdrodl-playbook) creates it from template)

# Run: 
## Help

`python -m ftp_operations --help`

## Check sum files

Prior to data files being transferred, a md5 check sum file `*.md5` will be created and added to the transfer list.
This will allow the data files (stored in PD) integrity to be checked against its adjacent .md5 files, by Aiflow or a script.

## Handling credentials

FTP credentials can be stored in config.yml file or given as script arguments:

**As script arguments:** (to allow for Airflow variables)

`python -m ftp_operations -ds IDRA -d 2023-03-27 --addr sftp.tudelft.nl --user YourNetID --pw YourPassword`


**In `config.yml`:** 

According to the following structure:

```yml
auth:
  host: sftp.tudelft.nl
  username: YourNetID
  password: YourPassword
```

## Data Sources 

**Information about data-file destinations can be found in [datasrcs.yml](datasrcs.yml)**

Note: ftp operations is installed from source, it packages [datasrcs.yml](datasrcs.yml) with the software. **Once [datasrcs.yml](datasrcs.yml) changes, ftp operations has to be re-installed** (`pip install -e .`), so that the new config gets packaged and the old version is discarded.

## Transfer files from host PC to FTP

IDRA:
* run (dry): `python -m ftp_operations  -ds IDRA -d 2023-03-27 --dry`
* run (not dry): `python -m ftp_operations -ds IDRA -d 2023-03-27`

PARsivel008: 
* `python -m ftp_operations  -ds PAR008 -d 2023-04-06 --dry`

Davis station BK:
* `python -m ftp_operations -ds DAVIS-BK -d 2023-04-06 --dry`

----

# Install as python package (Recommended when running on instrument host)

clone this repo, in instrument's host-computer  `cd /usr/local/src/` `git clone https://gitlab.tudelft.nl/ruisdael/ftp_operations.git` `cd ftp_operations` 

(optional) If using a virtual environment, activated virtual environment. 
* install: `pip3 install virtualenv `
* create venv: `python3 -m venv venv`
* activate: `source venv/bin/activate`

install python package: `pip install -e .`

try running package: `ftp_operations --help`

It might complain about config.yml not be in a certain directory, but you can do it yourself.

Locate the package (just for your information) `which ftp_operations`

# Run as cron job
With the path of ftp_operations, let's assume it is `/usr/local/src/ftp_operations/venv/bin/ftp_operations` 

Set cron job, every day at 1am 


`0 1 * * * /usr/local/src/ftp_operations/venv/bin/ftp_operations -ds DAVIS-BK -d $(date -u +"\%Y-\%m-\%d" --date="yesterday")`

* `0 1 * * *` at 1:00 run
* `-ds DAVIS-BK` data source: DAVIS-BK
* `$(date -u +"\%Y-\%m-\%d" --date="yesterday")` will return yesterday's date in format YY-mm-dd
   * in cron: percent-sign(%), unless escaped with backslash (\), will be changed into newline character


# ensure access to logs and data

**Log:**
* `mkdir /var/log/ftp_operations/`
* `chmod a+rw /var/log/ftp_operations/ -R`


# TODO:
- [ ] retry connection
- [X] prints instead of logs (for airflow)
- [X] password passed via command line (so that airflow vars can be used)
- [X] error handling transfer.py L 27

# Temporary choices
* Files are overwritten (Change L39-61 in [./transfer.py](./transfer.py))

python -m ftp_operations  -ds IDRA -d 2023-03-27 --dry

python -m ftp_operations -ds IDRA -d 2023-03-27


## going back in time with UNIX date

`for d in {1..40}; do datepast=`date -u +"%Y-%m-%d" --date="$d days ago" `; echo $datepast; done`
