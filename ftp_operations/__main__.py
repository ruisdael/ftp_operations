from argparse import ArgumentParser
from pathlib import Path
from ftp_operations.transfer import transfer2FTP
from ftp_operations.ftp_access import yaml2dict, get_credentials, connectToFTP
from ftp_operations.log import create_logger

parser = ArgumentParser(description="Ruisdael FTP Operations. \
                        Data structures are defined in datasrcs.yml")
parser.add_argument('--datasrc', '-ds',
                    help='The data source name given to the instrument/dataset in datasrcs.yml', 
                    required=True)
parser.add_argument('--date', '-d',
                    help='Date string for files to be captured. Format: YYYY-mm-dd',
                    required=True)
parser.add_argument('--dry',
                    help='Dry run: only lists files in source',
                    action="store_true")
parser.add_argument('--addr', '-a',
                    help='FTP host address',
                    required=False)
parser.add_argument('--user', '-u',
                    help='FTP username',
                    required=False)
parser.add_argument('--pw', '-p',
                    help='FTP user password',
                    required=False)


args = parser.parse_args()
def main():
    logger = create_logger('/var/log/ftp_operations/')
    logger.info('Running ftp_operations')
    wd = Path(__file__).parent.parent
    data_src = yaml2dict(path = wd / 'datasrcs.yml')[args.datasrc]

    if all((args.addr, args.user, args.pw)): # if all this stdin args have values
        logger.info('receiving credentials from command line')
        host, username, pswd = args.addr, args.user, args.pw # use stdin
    else:
        host, username, pswd = get_credentials() # from config.yml
        logger.info('reading credentials from config.yml')
    sftp = connectToFTP(host=host, username=username, pswd=pswd, logger=logger)
    transfer2FTP(sftp=sftp, 
                 data_src=data_src,
                 datestr=args.date,
                 dry=args.dry,
                 logger=logger)

if __name__ == '__main__':
    main()
