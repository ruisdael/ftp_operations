from pathlib import Path
from datetime import date, timedelta
import pysftp
import yaml

def get_credentials():
    wd = Path(__file__).parent.parent
    config = yaml2dict(path = wd / 'config.yml')
    host, username, pswd = config['auth']['host'], config['auth']['username'], config['auth']['password']
    return host, username, pswd

def parse_datetime():
    yesterday = date.today() - timedelta(days=1)
    y_m_d, y_m, y = yesterday.strftime("%Y%m%d"), yesterday.strftime("%Y%m"), yesterday.strftime("%Y")
    return y_m_d, y_m, y

def yaml2dict(path):
    with open(path, 'r') as yaml_f:
        yaml_content = yaml_f.read()
        yaml_dict = yaml.safe_load(yaml_content)
    return yaml_dict

def connectToFTP(host, username, pswd, logger): 
    # TODO: rewrite try/except logic
    retries = 3
    for i in range(retries):
        try: 
            cnopts = pysftp.CnOpts()
            cnopts.hostkeys = None
            sftp = pysftp.Connection(host, username=username, password=pswd, cnopts=cnopts)
            break
        except Exception as e:
            if i == (retries - 1):
                logger.error(f'SFTP CONNECTION COULD NOT BE ESTABLISHED: {e}')
                raise e
            else:
                pass
    logger.info(f'SFTP CONNECTION SUCCESSFULLY ESTABLISHED')
    return sftp