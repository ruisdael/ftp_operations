import logging
import os
import json
import time
import sys
from logging.handlers import TimedRotatingFileHandler
from pathlib import Path
from datetime import datetime



def log(log_path, log_name):
    logger = logging.getLogger(log_name)
    logging.Formatter.converter = time.gmtime  # set log time to utc/gmt
    log_json_format = logging.Formatter(
        json.dumps({'date': '%(asctime)s',
                    'name': '%(name)s',
                    'level': '%(levelname)s',
                    'msg': '%(message)s',
                    })
    )
    log_handler = TimedRotatingFileHandler(
        filename=log_path,
        when='midnight',
        backupCount=7,
        utc=True)
    log_handler.suffix = "%Y%m%d"
    log_handler.setFormatter(log_json_format)
    stdout_log_handler = logging.StreamHandler(sys.stdout)
    stdout_log_handler.setLevel(logging.INFO)
    log_stdout_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stdout_log_handler.setFormatter(log_stdout_format)
    logger.addHandler(log_handler)
    logger.addHandler(stdout_log_handler)    
    logger.setLevel(logging.DEBUG)
    return logger

def create_logger(logpath):
    log_dir = Path(logpath)
    if not os.path.exists(log_dir):
        os.mkdir(log_dir)
    log_file = log_dir / 'log.json'
    logger = log(log_path=log_file, 
                log_name=f"FTP OPERATIONS")  
    return logger

if __name__ == '__main__':
    log_dir = Path('/var/log/ftp_operations/')
    if not os.path.exists(log_dir):
        os.mkdir(log_dir)
    log_file = log_dir / 'log.json'
    logger = log(log_path=log_file, 
                log_name=f"FTP OPERATIONS")  
    logger.info(msg='hello info')
    logger.debug(msg='I am debugging')
    logger.error(msg='I am an an error')
