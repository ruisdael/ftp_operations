from ftp_operations.ftp_access import sftp
from stat import S_ISDIR 


def listDirRecurse(sftp, remote_path, data_file_extension):
    dirs_missing_data_files = []
    for attr in sftp.listdir_attr(remote_path):
        if S_ISDIR(attr.st_mode):
            # print('DIR:', remote_path) # , attr.filename)
            if remote_path[-1] == '/':
                dir_path = f'{remote_path}{attr.filename}'
            else:
                dir_path = f'{remote_path}/{attr.filename}'
            # print('next dir:', dir_path)
            listDirRecurse(sftp=sftp, remote_path=dir_path, data_file_extension=data_file_extension)
        else:
            dir_contents = [f for f in sftp.listdir(remote_path)]
            dir_contents_data_only = [f for f in dir_contents if f.endswith(data_file_extension) ]
            allfilespresent_bool = allfilespresent(dir_contents_data_only=dir_contents_data_only, expected_files_per_dir=24)
            if allfilespresent_bool == False:
                print(remote_path) # , attr.filename)
                dirs_missing_data_files.append(remote_path)
                find_missing(dir_contents=dir_contents_data_only, data_file_extension=data_file_extension)
            break

def allfilespresent(dir_contents_data_only, expected_files_per_dir):
    if len(dir_contents_data_only) == expected_files_per_dir:
        return True
    else: 
        return False

def find_missing(dir_contents, data_file_extension):
    dividor = '_'
    files_times = [((f.split(dividor))[-1]).replace(data_file_extension, '') for f in dir_contents]
    files_times_hours = [time[:2] for time in files_times]
    time_hours_reference = [str(i).zfill(2) for i in list(range(24))]
    missingtimes = list(set(time_hours_reference) - set(files_times_hours))
    print('missing files from hours:', missingtimes, '\n')

if __name__ == '__main__':
    print('DIRS W/ MISSING DATA FILES:\n')
    listDirRecurse(sftp=sftp, remote_path='/staff-umbrella/MRR/MRR005_Slufter/', data_file_extension='.nc') # remote path: end w/out slash
    sftp.close()
