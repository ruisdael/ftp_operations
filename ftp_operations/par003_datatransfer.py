#!/usr/bin bash

import glob
import os
from datetime import date, timedelta
import pysftp
import configparser

def get_credentials():
    config = configparser.ConfigParser()
    config.read('/c/scripts/sftp.ini')
    host, username, password = config['auth']['host'], config['auth']['username'], config['auth']['password']
    return host, username, password

def parse_datetime():
    yesterday = date.today() - timedelta(days=1)
    y_m_d, y_m, y = yesterday.strftime("%Y%m%d"), yesterday.strftime("%Y%m"), yesterday.strftime("%Y")
    return y_m_d, y_m, y

def put_folder_to_server(host, username, password,
                       local_path, remote_path):
    
    with pysftp.Connection(host, username=username, password=password, log='/mnt/c/ftp/par003/par003_sftp_log.log') as sftp:
        try:
            sftp.chdir(remote_path)  # Test if remote_path exists
        except IOError:
            sftp.mkdir(remote_path)  # Create remote_path
            sftp.chdir(remote_path)
        sftp.put_r(local_path, remote_path, preserve_mtime=True)    # At this point, you are in remote_path in either case


if __name__ == "__main__":
    host, username, password = get_credentials()
    y_m_d, y_m, y = parse_datetime()
    
    remote_path = f"/staff-umbrella/Parsivel/PAR003_GreenVillage/{y}/{y_m}/{y_m_d}"
    local_path = f"/mnt/c/ftp/par003/{y}/{y_m}/{y_m_d}/"
    
    put_folder_to_server(host, username, password,
                      local_path, remote_path)