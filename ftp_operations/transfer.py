import hashlib
import os
import subprocess
from glob import glob
from datetime import datetime
from pathlib import Path
from typing import ByteString


def compress_netcdf(nc_path: str, logger):
    # if no errors are found nccopy will compress netCDF
    nc_path = Path(nc_path)
    nc_path_compressed = nc_path.parent /  f'{nc_path.stem}_compressed.nc'
    try:
        subprocess.check_output(['nccopy', '-d6', nc_path, nc_path_compressed])
    except subprocess.CalledProcessError as e:
        logger.error(msg=f'Failed to compress {nc_path}. Error code:{e.returncode} ')
        os.remove(nc_path_compressed)
    else:
        logger.info(msg=f'Compressed netCDF {nc_path} successfully')
        # remove uncompressed and rename tmp to original filename
        os.remove(nc_path)
        os.rename(nc_path_compressed, nc_path)


def create_md5sum(content: ByteString) -> str:
    m = hashlib.md5(content)
    return m.hexdigest()


def transfer2FTP(sftp, data_src, datestr, dry, logger):
    '''
    def transfers files from data_src['src_dir_regex'] (in local machine) 
        to its data_src['dest_ftp_dir'] (in FTP server)
    - data_src['src_dir_regex'], data_src['extensions'] & datestr are used to locate the source files

    '''
    dateobjt = datetime.strptime(datestr, '%Y-%m-%d')  # '%Y-%m-d' is data-format provided by user in args
    dir_date_str = dateobjt.strftime(data_src['dir_date_format_str'])  # var dir_date_format_str in datasrcs.yml
    file_date_str = dateobjt.strftime(data_src['file_date_format_str'])  # var file_date_format_str in datasrcs.yml
    src_dir_regex = data_src['src_dir_regex'].replace('%dirdate%', dir_date_str)
    files_paths = []
    for fn_pattern in data_src['filename_patterns']:
        fn_pattern = fn_pattern.replace('%filedate%', file_date_str)
        src_path_regex = src_dir_regex[:-1] + fn_pattern  # removing last character (*) from src_dir_regex 
        fn_paths = glob(src_path_regex, recursive=True)
        files_paths += fn_paths
        logger.info(msg=f'File sensed for transfer: {fn_paths}')

    if len(files_paths) < 1:
        error_msg = f'No files with pattern {", ".join(data_src["filename_patterns"])} in src_dir: {src_dir_regex}'
        logger.error(error_msg)   
        raise ValueError(error_msg)
    
    # create md5 files for each data file
    for src_file_path in files_paths:
        with open(src_file_path, 'rb') as f_:
            hash = create_md5sum(content=f_.read())
            print(hash)
        with open(f'{src_file_path}.md5', 'w') as f_:
            f_.write(hash)

    # create daily/monthly/annual dir in destination based on data_src['dest_ftp_subdir_freq']
    # by subtracting with date_index None, days, or months from dir_date_str
    if data_src['dest_ftp_subdir_freq'] == 'daily':
        date_index = None
    elif data_src['dest_ftp_subdir_freq'] == 'monthly':
        date_index = -3
    elif data_src['dest_ftp_subdir_freq'] == 'yearly':
        date_index = -6
    
    if not dry:
        # create dir
        dest_ftp_subdir = str(Path(data_src['dest_ftp_dir'], str(dateobjt.year), dir_date_str))[:date_index]
        if not sftp.isdir(dest_ftp_subdir):
            logger.info(f'Making subdir {dest_ftp_subdir} in FTP')
            sftp.makedirs(dest_ftp_subdir, mode=666)
            sftp.cwd(dest_ftp_subdir)

        compress_netcdf(nc_path=src_file_path, logger=logger)
        
        # transfer
        logger.info(f'Starting Transfer process from {src_dir_regex} -> {data_src["dest_ftp_dir"]}')
        for src_file_path in files_paths:
            src_file_name = src_file_path.split('/')[-1]
            dest_file_path = str(Path(dest_ftp_subdir,src_file_name))
            # if not sftp.isfile(str(dest_file_path)): # only transfer if file is not present.
            logger.info(f'TRANSFER: {src_file_path} --> {dest_file_path}')
            # transfer to remote
            put_results = sftp.put(localpath=str(src_file_path), 
                                    remotepath=str(dest_file_path), # of file, not dir
                                    confirm=True,
                                    preserve_mtime=True)
            logger.info(f'TRANSFER RESULTS: {put_results}')
            # transfer m5sum
            src_md5_path = src_file_path + '.md5'
            dest_md5_path = str(Path(dest_ftp_subdir, src_file_name))+'.md5'
            put_results = sftp.put(localpath=str(src_md5_path),
                                   remotepath=str(dest_md5_path),
                                   confirm=True,
                                   preserve_mtime=True) 
            logger.info(f'TRANSFER: {src_md5_path} --> {dest_md5_path}')

            # else:
            #     logger.warning(f'NO TRANSFER: {src_file_path} already in destination {str(dest_ftp_subdir)}')            
            #     # TODO: add error

