import logging

# try:
#     1/0
# except BaseException:
#     logging.error("An exception was thrown!")

x = 9
if x > 10:
    pass
else:
    raise ValueError('x is not bigger than 10')
