import hashlib
from typing import ByteString


def create_md5sum(content: ByteString) -> str:
    m = hashlib.md5(content)
    return m.hexdigest()


with open('prototypes/hashthis.txt', 'rb') as f_:
    hash = create_md5sum(content=f_.read())
    print(hash)