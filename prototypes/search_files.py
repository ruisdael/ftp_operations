from pathlib import Path
from datetime import timedelta, datetime
from glob import glob
import yaml

def yaml2dict(path):
    with open(path, 'r') as yaml_f:
        yaml_content = yaml_f.read()
        yaml_dict = yaml.safe_load(yaml_content)
    return yaml_dict

def get_data_source_dict(instance):
    wd = Path(__file__).parent.parent
    data_srcs = yaml2dict(path = wd / 'datasrcs.yml')
    print(data_srcs[instance])
    return data_srcs[instance]


def get_files(data_src, date_objt):
    '''
    returns list of full-paths for the files that follow the data_source's src_dir_regex, extensions and dir_date_format_str
    in datasrcs.yml
    * data_src: dict[data_source] from datasrcs.yml
    * date_obj: a date object (rather than str, so it be formatted according to 
    data_src['dir_date_format_str'] )
    '''
    dir_date_format_str = date_objt.strftime(data_src['dir_date_format_str'])
    src_dir_regex = data_src['src_dir_regex'].replace('%date%', dir_date_format_str)
    print('src_dir_regex', src_dir_regex)
    files_path = []
    for ext in data_src['extensions']:
        src_dir_regex_w_ext = src_dir_regex + ext
        path = glob(src_dir_regex_w_ext, recursive=True)
        files_path += path
    return files_path
    

yesterday = datetime.now() - timedelta(hours=48)  # this date should be given script arguments
data_src=get_data_source_dict(instance='IDRA')
files_paths = get_files(data_src=get_data_source_dict(instance='IDRA',),
                       date_objt=yesterday)
print(files_paths)