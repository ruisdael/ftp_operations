from setuptools import setup
  
setup(
    name='ftp_operations',
    version='0.1',
    description='A Python package to do FTP transfers and detect missing data',
    author='Andre Castro',
    author_email='a.i.castro@tudelft.nl',
    packages=['ftp_operations'],
    install_requires=[
        'pysftp',
        'PyYAML',
    ],
)
